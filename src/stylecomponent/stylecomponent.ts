import { Vue, Component, Watch, Prop } from 'vue-property-decorator';
import { ScreenText } from '@/lang/ScreenText';
import APP_UTILITIES from "@/utilities/commonFunctions";
import AccountListComponent from '@/components/accountlistcomponent/AccountListComponent.vue';
import AdminDashboardComponent from '@/components/admindashboardcomponent/AdminDashboardComponent.vue';



@Component({
    components: {
        'account-list': AccountListComponent,
        'admin-dashboard': AdminDashboardComponent,
    }
})
export default class RootComponent extends Vue {
    private objScreenText: ScreenText = new ScreenText();
    public userRoles : any = []; 
    public currentRoleId : number = 0;

    @Prop()
    userDetails!: Object;

    /**
    * method 'getScreenText' responsible to get the localize screen text
    * @param {string} key
    * @return {string} screen text
    */

    public getScreenText(key: string): string {
        return this.objScreenText.getScreenText(key);
    }

    beforeMount(){
        this.getUserRoles();
        // document.cookie = `accountId=;=; expires=${new Date()}; path=/;`;
    }

    //[{"id":14,"roleId":2,"accountId":0,"programId":0,"siteId":0}]
    public getUserRoles(){
        let userRoles: any = APP_UTILITIES.getCookie("user_role");
        this.userRoles = JSON.parse(userRoles);
        this.userRoles.forEach((item: any, index: number) => {    
            if (item.hasOwnProperty("roleId")) {
                this.currentRoleId = item.roleId;
            }
        })
    }
}

